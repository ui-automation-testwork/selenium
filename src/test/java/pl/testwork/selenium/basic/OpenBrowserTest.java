package pl.testwork.selenium.basic;

import org.junit.jupiter.api.Test;

public class OpenBrowserTest extends BaseTest {

    @Test
    public void openBrowser() {

        webDriver.navigate().to("https://the-internet.herokuapp.com/");
    }

}
