package pl.testwork.selenium.basic;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileUploadTest extends BaseTest {

    @Test
    public void uploadFile() {
        openTheInternetPage("upload");
        String fileName = "some_file.txt";
        File fileToUpload = getFile(fileName);

        WebElement uploadForm = webDriver.findElement(By.id("file-upload"));
        uploadForm.sendKeys(fileToUpload.getAbsolutePath());
        uploadForm.submit();

        WebElement uploadedFile = webDriver.findElement(By.id("uploaded-files"));
        assertTrue(uploadedFile.getText().contains(fileToUpload.getName()));
    }
}
