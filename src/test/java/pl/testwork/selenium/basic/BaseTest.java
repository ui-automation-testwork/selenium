package pl.testwork.selenium.basic;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public class BaseTest {
    protected WebDriver webDriver;
    public static Config frameworkConfig;

    @BeforeAll
    public static void readConfig() {
        String environment = ofNullable(System.getenv("TEST_ENV")).orElse("local");
        Config defaultConfig = ConfigFactory.parseResources("config/default.conf");
        Config envConfig = ConfigFactory.parseResources(String.format("config/%s.conf", environment));
        frameworkConfig = ConfigFactory.load().withFallback(envConfig).withFallback(defaultConfig);
    }

    @BeforeEach
    public void setUp() {
        openChrome();
    }

    protected void openChrome() {
        ChromeOptions chromeOptions = new ChromeOptions();
        if (frameworkConfig.getBoolean("browser.headless")) {
            chromeOptions.addArguments("--headless");
        }
        webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().window().maximize();
    }

    @AfterEach
    public void closeBrowser() {
        webDriver.quit();
    }

    protected void openTheInternetPage(String page) {
        webDriver.navigate().to(frameworkConfig.getString("sut.url") + page);
    }

    protected File getFile(String filename) {
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(requireNonNull(classLoader.getResource(filename)).getFile());
    }
}
