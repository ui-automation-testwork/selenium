package pl.testwork.selenium.basic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class TimeoutsTest extends BaseTest {

    @Test
    public void pageLoadTimeoutException() {
        webDriver.manage().timeouts().pageLoadTimeout(Duration.of(1, ChronoUnit.MILLIS));

        Assertions.assertThrows(TimeoutException.class,
                () -> webDriver.navigate().to("https://the-internet.herokuapp.com/"));
    }

    @Test
    public void implicitWait() {
        //Implicit wait timeout is applied to all find... methods until browser is closed. Default vale is 0.
        webDriver.manage().timeouts().implicitlyWait(Duration.of(2, ChronoUnit.SECONDS));

        webDriver.navigate().to("https://the-internet.herokuapp.com/");

        Assertions.assertThrows(NoSuchElementException.class, () -> webDriver.findElement(By.id("no-such-element")));
    }

    @Test
    public void explicitWait() {
        webDriver.navigate().to("https://the-internet.herokuapp.com/");

        WebDriverWait wait = new WebDriverWait(webDriver, Duration.of(2, ChronoUnit.SECONDS));

        Assertions.assertThrows(TimeoutException.class,
                () -> wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("no-such-element"))));
    }

    @Test
    public void fluentWait() {
        webDriver.navigate().to("https://the-internet.herokuapp.com/");

        FluentWait<WebDriver> wait = new FluentWait<>(webDriver)
                .pollingEvery(Duration.of(100, ChronoUnit.MILLIS))
                .withTimeout(Duration.of(2, ChronoUnit.SECONDS))
                .ignoring(NoSuchElementException.class);

        Assertions.assertThrows(TimeoutException.class,
                () -> wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("no-such-element"))));
    }

    @Test
    public void scriptTimeout() {
        webDriver.manage().timeouts().scriptTimeout(Duration.of(1, ChronoUnit.SECONDS));
        webDriver.navigate().to("https://the-internet.herokuapp.com/");

        Assertions.assertThrows(ScriptTimeoutException.class,
                () -> ((JavascriptExecutor) webDriver).executeScript("await new Promise(r => setTimeout(r, 2000));"));
    }

}
