package pl.testwork.selenium.basic;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class FileDownloadTest extends BaseTest {

    @TempDir
    File temDir;

    @Override
    public void openChrome() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        Map<String, Object> preferences = new HashMap<>();
        preferences.put("download.default_directory", temDir.getAbsolutePath());
        chromeOptions.setExperimentalOption("prefs", preferences);
        webDriver = new ChromeDriver(chromeOptions);
    }

    @Test
    public void downloadFile() {
        // This test depends on presence of given file, run FileUploadTest first to make sure it is true.
        String fileName = "some_file.txt";
        openTheInternetPage("download");

        WebElement downloadLink = webDriver.findElement(By.linkText(fileName));
        downloadLink.click();

        File downloadedFile = Paths.get(temDir.getAbsolutePath() + "/" + fileName).toFile();
        Awaitility.await().until(downloadedFile::exists);
        assertThat(downloadedFile).exists();
    }

}
